﻿#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <iostream>
#include <cstdlib>


class HelloTriangleApplication {
public:
	void run() {
		initWindow();
		initVulkan();
		mainLoop();
		cleanup();
	}
private:
	void initWindow() {
		//应为glfw默认会创建一个opengl上下文，此处通知不创建
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		//设置窗口标志
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	}

	void initVulkan() {

	}

	void mainLoop() {

	}

	void cleanup() {

	}
};

int main() {
	HelloTriangleApplication app;
	try {
		app.run();
	}
	catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}